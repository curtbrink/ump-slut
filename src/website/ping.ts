import {Message, Snowflake, TextChannel} from "discord.js";
import {createNewABPing} from "../ab-ping-orchestrator";
import {
    SNOWFLAKE_CHANNEL_AB_PINGS,
    SNOWFLAKE_CHANNEL_UMP_PINGS,
    SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN,
} from "../important-constants";

export async function handleSiteABPingRequest(msg: Message, snowflake: Snowflake, name: string, milr: boolean, id36: string, pingers: string[]) {
    let targetChannel =
        msg.guild.id === SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN ?
            msg.guild.channels.get(SNOWFLAKE_CHANNEL_AB_PINGS) as TextChannel :
            msg.channel as TextChannel;

    let responseChannel =
        msg.guild.id === SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN ?
            msg.guild.channels.get(SNOWFLAKE_CHANNEL_UMP_PINGS) as TextChannel :
            msg.channel as TextChannel;

    await createNewABPing(targetChannel, responseChannel, snowflake, name, milr, pingers, id36);
}

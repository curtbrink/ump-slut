import {DMChannel, Message} from "discord.js";
import {commandMap} from "../index";
import {Command} from "./command";

/**
 * Generates a help message and DMs it to the user.
 */
class HelpCommandClass extends Command {

    description(): string {
        return "Provides this information page.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string> {
        let helpMessage = `**__Ump Slut Commands__**`;
        for (let commandName of Object.keys(commandMap)) {
            let command = commandMap[commandName] as Command;
            if (!command.hidden() && !command.nlCentral())
                helpMessage += `\n\`.${commandName}\`: ${command.description()}`;
        }

        await msg.author.send(helpMessage);
        return !(msg.channel instanceof DMChannel) ? `I've DM'd that info to you!` : undefined;
    }

}

export const HelpCommand = new HelpCommandClass();

import {Message, RichEmbed} from "discord.js";
import {setBotSetting} from "../database";
import {DEVELOPMENT_MODE, SNOWFLAKE_CHANNEL_OOTC} from "../important-constants";
import {Command} from "./command";

const VALID_SETTINGS = ["scoreboard.session"];

class BotSettingClass extends Command {

    description(): string {
        return "Links your Discord account to your MLR player.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        let userAuthorized = DEVELOPMENT_MODE || msg.channel.id === SNOWFLAKE_CHANNEL_OOTC;
        if (!userAuthorized)
            return "You can't do that.";

        let setting = args.shift();
        let value = args.shift();

        if (!setting || !value)
            return "`.botsetting name value`";

        if (!VALID_SETTINGS.includes(setting))
            return `That is an invalid setting name. Valid options are:\n\`\`\`\n${VALID_SETTINGS.join("\n")}\`\`\``;

        await setBotSetting(setting, value);
        return "Done.";
    }
}

export const BotSetting = new BotSettingClass();

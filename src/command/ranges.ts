import {Message, RichEmbed} from "discord.js";
import {getUserSetting} from "../database";
import {PlayerSetting} from "../important-constants";
import {getPark, getPlayer} from "../site-pull";
import {calculateRanges} from "../SwingStat";
import {Command} from "./command";

/**
 * Provides swing ranges between two players on a specific field.
 */
class RangesCommandClass extends Command {

    description(): string {
        return "Calculates outcome ranges between a batter and a pitcher.";
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        // Undo the args split by spaces, and instead split by semicolon
        let argsT = args.join(" ");
        args = argsT.split(";");

        // Make sure we have enough pieces
        if (args.length < 2) {
            return `\`.ranges batter; pitcher[; park]\``;
        }

        // Get the pieces
        let batterQuery = args[0].trim();
        let pitcherQuery = args[1].trim();
        let parkQuery = args[2];
        if (parkQuery === undefined)
            parkQuery = "Neutral Park";

        // Make sure they're all valid
        let batter = await getPlayer(batterQuery);
        if (typeof batter === "string") {
            return batter;
        }

        let pitcher = await getPlayer(pitcherQuery);
        if (typeof pitcher === "string") {
            return pitcher;
        }

        let park = await getPark(parkQuery.trim());
        if (typeof park === "string")
            return park;

        // Calculate range
        let calculatedRange = calculateRanges(batter, pitcher, park);

        // Make the embed message format
        let description = "";
        if (pitcher.positionPrimary !== "P" && batter.positionPrimary === "P") // Maybe they got it backwards
            description += `**WARNING:** ${pitcher.name} is a position pitcher. Did you mean:\n\`.swing ${pitcherQuery}; ${batterQuery}; ${parkQuery}\`\n\n`;
        description += `${pitcher.name} pitching against ${batter.name} at ${park.name}.`;
        let ret: string | RichEmbed;

        if (await getUserSetting(msg.author.id, PlayerSetting.RangesCompact, false)) {
            ret = description + "\`\`\`";
            let runningTotal = 0;
            let ranges = [calculatedRange.rangeHR, calculatedRange.range3B, calculatedRange.range2B, calculatedRange.range1B, calculatedRange.rangeBB, calculatedRange.rangeFO, calculatedRange.rangeK, calculatedRange.rangePO, calculatedRange.rangeRGO, calculatedRange.rangeLGO];
            let outcomes = ["HR", "3B", "2B", "1B", "BB", "FO", "K", "PO", "RGO", "LGO"];
            for (let i = 0; i < ranges.length; i++) {
                if (ranges[i] === 0)
                    ret += `${outcomes[i].padStart(3, " ")}: ---\n`;
                else
                    ret += `${outcomes[i].padStart(3, " ")}: ${runningTotal.toString().padStart(3, "0")} - ${(runningTotal + ranges[i] - 1).toString().padStart(3, "0")}\n`;
                runningTotal += ranges[i];
            }
            ret += "```";
        } else {
            ret = new RichEmbed({
                description,
                fields: [],
            });
            let runningTotal = 0;
            let ranges = [calculatedRange.rangeHR, calculatedRange.range3B, calculatedRange.range2B, calculatedRange.range1B, calculatedRange.rangeBB, calculatedRange.rangeFO, calculatedRange.rangeK, calculatedRange.rangePO, calculatedRange.rangeRGO, calculatedRange.rangeLGO];
            let outcomes = ["HR", "3B", "2B", "1B", "BB", "FO", "K", "PO", "RGO", "LGO"];
            for (let i = 0; i < ranges.length; i++) {
                if (ranges[i] === 0)
                    ret.addField(outcomes[i], "---", true);
                else
                    ret.addField(outcomes[i], `${runningTotal} - ${runningTotal + ranges[i] - 1}`, true);
                runningTotal += ranges[i];
            }
        }

        return ret;
    }

}

export const RangesCommand = new RangesCommandClass();

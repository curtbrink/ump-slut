import * as Discord from "discord.js";
import {Message, RichEmbed, TextChannel} from "discord.js";
import * as rp from "request-promise";
import {handlePingReaction} from "./ab-ping-orchestrator";
import {BotSetting} from "./command/botsetting";
import {ClaimCommand} from "./command/claim";
import {Command} from "./command/command";
import {DraftCommand, DraftTimerCommand} from "./command/draft";
import {HelpCommand} from "./command/help";
import {InviteCommand} from "./command/invite";
import {HandbookCommand, RulebookCommand} from "./command/link-commands";
import {LookupCommand} from "./command/lookup";
import {MyGamesCommand} from "./command/my-games";
import {
    Alt,
    Balk,
    Belle,
    Birds,
    CatGirl,
    F5,
    Franki, FreeAgent,
    NLHelp,
    SexBot,
    Sneeze,
    SpaceRace,
    SpamNLCentral,
    SteveHarvey,
} from "./command/nl-central-shit";
import {PingCommand} from "./command/ping";
import {
    PingGMOptInCommand,
    PingGMOptOutCommand,
    PingOptInCommand,
    PingOptOutCommand,
    UmpPingBackOptInCommand,
    UmpPingBackOptOutCommand,
} from "./command/ping-optin-commands";
import {PlayerCommand} from "./command/player";
import {PollCommand} from "./command/poll";
import {RangesCommand} from "./command/ranges";
import {RoleMeCommand, RoleMeMiLRCommand} from "./command/roleme";
import {ScoreboardCommand} from "./command/scoreboard";
import {SettingCommand} from "./command/setting";
import {
    BStatsCommand,
    MiLRBStatsCommand,
    MiLRPStatsCommand,
    MiLRStatsCommand,
    PStatsCommand,
    S3BStatsCommand,
    S3PStatsCommand,
    S3StatsCommand,
    StatsCommand,
} from "./command/stats";
import {SwingCommand} from "./command/swing";
import {TallyCommand} from "./command/tally";
import {getBotSetting, setBotSetting} from "./database";
import {
    DEVELOPMENT_MODE,
    EMBED_FOOTER,
    SNOWFLAKE_CHANNEL_NL_CENTRAL,
    SNOWFLAKE_CHANNEL_OOTC,
    SNOWFLAKE_CHANNEL_SCOREBOARD, SNOWFLAKE_CHANNEL_SITE_COMMUNICATION,
    SNOWFLAKE_CHANNEL_UMP_PINGS,
    SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN,
    SNOWFLAKE_USER_LLAMOS,
} from "./important-constants";
import {init} from "./ozzie-albies";
import {handleSiteMessage} from "./website/handler";

const DISCORD_TOKEN = process.env.DISCORD_TOKEN;
const discordClient = new Discord.Client();
const prefix = ".";

// Log when we make a connection to Discord
discordClient.on("ready", async () => {
    if (!DEVELOPMENT_MODE) {
        await init(discordClient);
        await updateScoreboard();
    }
    await discordClient.user.setPresence({afk: false, game: {name: "Baseball", type: "PLAYING"}, status: "online"});
    console.log("Logged in!");
});

// Maps the command names to the command instances
export const commandMap = {
    player: PlayerCommand,
    botsetting: BotSetting,
    draft: DraftCommand,
    drafttimer: DraftTimerCommand,
    swing: SwingCommand,
    handbook: HandbookCommand,
    rulebook: RulebookCommand,
    claim: ClaimCommand,
    ping: PingCommand,
    ranges: RangesCommand,
    mygames: MyGamesCommand,
    pingoptin: PingOptInCommand,
    pingoptout: PingOptOutCommand,
    gmpingoptin: PingGMOptInCommand,
    gmpingoptout: PingGMOptOutCommand,
    umppingoptout: UmpPingBackOptOutCommand,
    umppingoptin: UmpPingBackOptInCommand,
    invite: InviteCommand,
    scoreboard: ScoreboardCommand,
    roleme: RoleMeCommand,
    rolememilr: RoleMeMiLRCommand,
    lookup: LookupCommand,
    setting: SettingCommand,
    stats: StatsCommand,
    milrstats: MiLRStatsCommand,
    bstats: BStatsCommand,
    pstats: PStatsCommand,
    milrbstats: MiLRBStatsCommand,
    milrpstats: MiLRPStatsCommand,
    stats3: S3StatsCommand,
    bstats3: S3BStatsCommand,
    pstats3: S3PStatsCommand,
    tally: TallyCommand,
    poll: PollCommand,
    help: HelpCommand,
    // Here be NL Central
    nlhelp: NLHelp,
    franki: Franki,
    birds: Birds,
    nlcentral: SpamNLCentral,
    catgirl: CatGirl,
    steveharvey: SteveHarvey,
    sneeze: Sneeze,
    spacerace: SpaceRace,
    f5: F5,
    sexbot: SexBot,
    belle: Belle,
    balk: Balk,
    freeagent: FreeAgent,
    alt: Alt,
};

discordClient.on("message", async msg => {
    if (msg.channel.id === SNOWFLAKE_CHANNEL_SITE_COMMUNICATION)
        await handleSiteMessage(msg);

    if (!msg.content.startsWith(prefix) || msg.author.bot) {
        return;
    }

    const args = msg.content.slice(prefix.length).split(/ +/);
    const commandLabel = args.shift().toLowerCase();

    // Find command to call
    let command: Command = commandMap[commandLabel];

    // Make sure it isn't a hidden one called in the wrong channel
    if (command !== undefined) {
        if ((command.hidden() && msg.channel.id !== "593901709198098442" && msg.channel.id !== SNOWFLAKE_CHANNEL_UMP_PINGS && msg.channel.id !== SNOWFLAKE_CHANNEL_OOTC) && !DEVELOPMENT_MODE)
            return;
        if (command.nlCentral() && msg.channel.id !== SNOWFLAKE_CHANNEL_NL_CENTRAL)
            return;
    }

    // Now do nothing if we don't know the command
    if (command === undefined)
        return;

    // Call the command
    command.handleCommand(args, msg)
        .then(response => {
            if (response === undefined)
                return;

            // Append the "If something's wrong" footer if it's an Embed
            if (response instanceof RichEmbed) {
                response.footer = EMBED_FOOTER;
            }

            // Send the response
            msg.channel.send(response)
               .catch(console.error);
        }).catch(e => {
            // DMs me (Llamos) when there's an unhandled error in a command on production.
            if (!DEVELOPMENT_MODE) {
                discordClient.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN).members.get(SNOWFLAKE_USER_LLAMOS).send("An error in console for you, sir.").catch(console.error);
                console.error(e);
                msg.channel.send(`Something went wrong. I've already notified Llamos (the author), so please try again later.`)
                   .catch(console.error);
            } else {
                console.error(e);
                msg.channel.send("Something went wrong. Check console.")
                   .catch(console.error);
            }
        });
});

// Login to Discord, starts background sockets
discordClient.login(DISCORD_TOKEN)
             .catch(console.error);

console.log(`Development Mode: ${DEVELOPMENT_MODE ? "Yes" : "No"}`);

discordClient.on("messageReactionAdd", async (messageReaction, user) => {
    // Make sure the bot wrote the message to be reacted to
    if (messageReaction.message.author.id === discordClient.user.id) {
        // For baseball reaction, should be .ping
        if (messageReaction.emoji.name === "⚾") {
            await handlePingReaction(messageReaction.message, user);
        }
    }
});

if (!DEVELOPMENT_MODE)
    setInterval(() => updateScoreboard().catch(console.error), 300000);

async function updateScoreboard() {
    let setting = await getBotSetting("scoreboard.session");
    let season = setting.split(".")[0];
    let session = setting.split(".")[1];

    let reqOptions = {
        uri: `https://redditball.xyz/api/v1/games/${season}/${session}`,
        json: true,
    };

    let games = await rp(reqOptions);
    let embed = new RichEmbed();

    embed.setTitle("FakeBaseball Scoreboard");
    embed.setColor(0xffffff);
    embed.setAuthor("FakeBaseball", undefined, "https://redditball.xyz");
    embed.setTimestamp(new Date());
    embed.setFooter("Updated Automatically");

    for (let game of games) {
        let title = `${setting}: ${game.awayTeam.tag} ${game.awayScore} - ${game.homeScore} ${game.homeTeam.tag}: ${game.completed ? "FINAL" : `${game.thirdOccupied ? "x" : "\\_"} ${game.secondOccupied ? "ˣ" : "-"} ${game.firstOccupied ? "x" : "\\_"} | ${game.inning} ${game.outs} Out`}`;
        let description = `[View Game](https://redditball.xyz/games/${game.id})`;
        embed.addField(title, description);
    }

    let message = await getBotSetting("scoreboard.message");
    let guild = discordClient.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN);
    let channel = guild.channels.get(SNOWFLAKE_CHANNEL_SCOREBOARD) as TextChannel;
    if (message !== undefined) {
        let existingMessage: Message;
        try {
            existingMessage = await channel.fetchMessage(message);
        } catch (e) {
            await channel.bulkDelete(100, true);
            console.log("Couldn't find scoreboard message, recreating....");
        }
        if (existingMessage !== undefined) {
            await existingMessage.edit("", embed);
            return;
        }
    }

    let createdMessage: Message = await (discordClient.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN).channels.get(SNOWFLAKE_CHANNEL_SCOREBOARD) as TextChannel).send(embed) as Message;
    await setBotSetting("scoreboard.message", createdMessage.id);
}

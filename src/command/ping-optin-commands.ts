import {GuildChannel, Message} from "discord.js";
import {setUserSetting} from "../database";
import {DEVELOPMENT_MODE, PlayerSetting, SNOWFLAKE_CHANNEL_UMP_PINGS, SNOWFLAKE_ROLE_GM} from "../important-constants";
import {Command} from "./command";

class PingOptInCommandsClass extends Command {

    gm: boolean;
    ump: boolean;
    optin: boolean;

    constructor(gm: boolean, ump: boolean, optin: boolean) {
        super();
        this.gm = gm;
        this.ump = ump;
        this.optin = optin;
    }

    description(): string {
        return `Opts ${this.optin ? "in to receive" : "out of receiving"} at-bat ${this.ump ? "response " : ""}pings${this.gm ? " for unavailable teammates" : ""}.`;
    }

    async handleCommand(args: string[], msg: Message): Promise<string> {
        // Check if GM mode and the player is a GM
        if (this.gm && msg.channel instanceof GuildChannel && !msg.member.roles.has(SNOWFLAKE_ROLE_GM) && !DEVELOPMENT_MODE)
            return `You aren't a GM, so you can't do that.`;

        // Check if ump mode and player is ump
        if (this.ump && msg.channel.id !== SNOWFLAKE_CHANNEL_UMP_PINGS && !DEVELOPMENT_MODE)
            return `You aren't an ump, so you can't do that.`;

        // Change the setting
        await setUserSetting(msg.author.id, this.gm ? PlayerSetting.PingOptInGM : this.ump ? PlayerSetting.PingOptInUmp : PlayerSetting.PingOptIn, this.optin);

        // Return a confirmation
        return this.gm ?
            `You've opted ${this.optin ? "**in** to receive" : "**out** of receiving"} pings for team members that aren't available on Discord.` :
            (this.ump ? `You've opted ${this.optin ? "**in** to receive" : "**out** of receiving"} pings from batters.` :
                `You've opted ${this.optin ? "**in** to receive" : "**out** of receiving"} at-bat pings.`);
    }

}

export const PingOptInCommand = new PingOptInCommandsClass(false, false, true);
export const PingOptOutCommand = new PingOptInCommandsClass(false, false, false);
export const PingGMOptInCommand = new PingOptInCommandsClass(true, false, true);
export const PingGMOptOutCommand = new PingOptInCommandsClass(true, false, false);
export const UmpPingBackOptInCommand = new PingOptInCommandsClass(false, true, true);
export const UmpPingBackOptOutCommand = new PingOptInCommandsClass(false, true, false);
